// Fill out your copyright notice in the Description page of Project Settings.

#include "Cplus_actor.h"


// Sets default values
ACplus_actor::ACplus_actor(const FObjectInitializer& ObjetcInitializer)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ACplus_actor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACplus_actor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

