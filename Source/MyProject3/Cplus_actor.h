// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Cplus_actor.generated.h"

UENUM(BlueprintType)
enum class EExample : uint8
{
	E_RED UMETA(DisplayNAme="RED"),
	E_GREEN UMETA(DisplayNAme = "GREEN"),
	E_BLUE UMETA(DisplayNAme = "BLUE"),
};

USTRUCT(BlueprintType)
struct FMyData 
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	EExample EnumValue;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 intValue;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FString StringValue;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		AActor* ActorPtr;

	FMyData()
	{
		EnumValue	= EExample::E_RED;
		intValue	= 10;
		StringValue = TEXT("MyString");
		ActorPtr	= nullptr;
	}
};

UCLASS()
class MYPROJECT3_API ACplus_actor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACplus_actor(const FObjectInitializer& ObjetcInitializer);

	UPROPERTY(BlueprintReadOnly)
	EExample ExampleEnumValue;

	UPROPERTY(BlueprintReadWrite)
	EExample ExampleStructValue;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
};
