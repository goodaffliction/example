// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef MYPROJECT3_Cplus_actor_generated_h
#error "Cplus_actor.generated.h already included, missing '#pragma once' in Cplus_actor.h"
#endif
#define MYPROJECT3_Cplus_actor_generated_h

#define MyProject3_Source_MyProject3_Cplus_actor_h_20_GENERATED_BODY \
	friend MYPROJECT3_API class UScriptStruct* Z_Construct_UScriptStruct_FMyData(); \
	MYPROJECT3_API static class UScriptStruct* StaticStruct();


#define MyProject3_Source_MyProject3_Cplus_actor_h_43_RPC_WRAPPERS
#define MyProject3_Source_MyProject3_Cplus_actor_h_43_RPC_WRAPPERS_NO_PURE_DECLS
#define MyProject3_Source_MyProject3_Cplus_actor_h_43_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesACplus_actor(); \
	friend MYPROJECT3_API class UClass* Z_Construct_UClass_ACplus_actor(); \
public: \
	DECLARE_CLASS(ACplus_actor, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/MyProject3"), NO_API) \
	DECLARE_SERIALIZER(ACplus_actor) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define MyProject3_Source_MyProject3_Cplus_actor_h_43_INCLASS \
private: \
	static void StaticRegisterNativesACplus_actor(); \
	friend MYPROJECT3_API class UClass* Z_Construct_UClass_ACplus_actor(); \
public: \
	DECLARE_CLASS(ACplus_actor, AActor, COMPILED_IN_FLAGS(0), 0, TEXT("/Script/MyProject3"), NO_API) \
	DECLARE_SERIALIZER(ACplus_actor) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define MyProject3_Source_MyProject3_Cplus_actor_h_43_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ACplus_actor(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ACplus_actor) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACplus_actor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACplus_actor); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACplus_actor(ACplus_actor&&); \
	NO_API ACplus_actor(const ACplus_actor&); \
public:


#define MyProject3_Source_MyProject3_Cplus_actor_h_43_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACplus_actor(ACplus_actor&&); \
	NO_API ACplus_actor(const ACplus_actor&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACplus_actor); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACplus_actor); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ACplus_actor)


#define MyProject3_Source_MyProject3_Cplus_actor_h_43_PRIVATE_PROPERTY_OFFSET
#define MyProject3_Source_MyProject3_Cplus_actor_h_40_PROLOG
#define MyProject3_Source_MyProject3_Cplus_actor_h_43_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MyProject3_Source_MyProject3_Cplus_actor_h_43_PRIVATE_PROPERTY_OFFSET \
	MyProject3_Source_MyProject3_Cplus_actor_h_43_RPC_WRAPPERS \
	MyProject3_Source_MyProject3_Cplus_actor_h_43_INCLASS \
	MyProject3_Source_MyProject3_Cplus_actor_h_43_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define MyProject3_Source_MyProject3_Cplus_actor_h_43_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	MyProject3_Source_MyProject3_Cplus_actor_h_43_PRIVATE_PROPERTY_OFFSET \
	MyProject3_Source_MyProject3_Cplus_actor_h_43_RPC_WRAPPERS_NO_PURE_DECLS \
	MyProject3_Source_MyProject3_Cplus_actor_h_43_INCLASS_NO_PURE_DECLS \
	MyProject3_Source_MyProject3_Cplus_actor_h_43_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID MyProject3_Source_MyProject3_Cplus_actor_h


#define FOREACH_ENUM_EEXAMPLE(op) \
	op(EExample::E_RED) \
	op(EExample::E_GREEN) \
	op(EExample::E_BLUE) 
PRAGMA_ENABLE_DEPRECATION_WARNINGS
