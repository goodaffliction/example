// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "GeneratedCppIncludes.h"
#include "Cplus_actor.h"
PRAGMA_DISABLE_OPTIMIZATION
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCplus_actor() {}
// Cross Module References
	MYPROJECT3_API UEnum* Z_Construct_UEnum_MyProject3_EExample();
	UPackage* Z_Construct_UPackage__Script_MyProject3();
	MYPROJECT3_API UScriptStruct* Z_Construct_UScriptStruct_FMyData();
	ENGINE_API UClass* Z_Construct_UClass_AActor_NoRegister();
	MYPROJECT3_API UClass* Z_Construct_UClass_ACplus_actor_NoRegister();
	MYPROJECT3_API UClass* Z_Construct_UClass_ACplus_actor();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
// End Cross Module References
static UEnum* EExample_StaticEnum()
{
	static UEnum* Singleton = nullptr;
	if (!Singleton)
	{
		Singleton = GetStaticEnum(Z_Construct_UEnum_MyProject3_EExample, Z_Construct_UPackage__Script_MyProject3(), TEXT("EExample"));
	}
	return Singleton;
}
static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EExample(EExample_StaticEnum, TEXT("/Script/MyProject3"), TEXT("EExample"), false, nullptr, nullptr);
	UEnum* Z_Construct_UEnum_MyProject3_EExample()
	{
		UPackage* Outer = Z_Construct_UPackage__Script_MyProject3();
		extern uint32 Get_Z_Construct_UEnum_MyProject3_EExample_CRC();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EExample"), 0, Get_Z_Construct_UEnum_MyProject3_EExample_CRC(), false);
		if (!ReturnEnum)
		{
			ReturnEnum = new(EC_InternalUseOnlyConstructor, Outer, TEXT("EExample"), RF_Public|RF_Transient|RF_MarkAsNative) UEnum(FObjectInitializer());
			TArray<TPair<FName, int64>> EnumNames;
			EnumNames.Emplace(TEXT("EExample::E_RED"), 0);
			EnumNames.Emplace(TEXT("EExample::E_GREEN"), 1);
			EnumNames.Emplace(TEXT("EExample::E_BLUE"), 2);
			EnumNames.Emplace(TEXT("EExample::E_MAX"), 3);
			ReturnEnum->SetEnums(EnumNames, UEnum::ECppForm::EnumClass);
			ReturnEnum->CppType = TEXT("EExample");
#if WITH_METADATA
			UMetaData* MetaData = ReturnEnum->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnEnum, TEXT("BlueprintType"), TEXT("true"));
			MetaData->SetValue(ReturnEnum, TEXT("E_BLUE.DisplayNAme"), TEXT("BLUE"));
			MetaData->SetValue(ReturnEnum, TEXT("E_GREEN.DisplayNAme"), TEXT("GREEN"));
			MetaData->SetValue(ReturnEnum, TEXT("E_RED.DisplayNAme"), TEXT("RED"));
			MetaData->SetValue(ReturnEnum, TEXT("ModuleRelativePath"), TEXT("Cplus_actor.h"));
#endif
		}
		return ReturnEnum;
	}
	uint32 Get_Z_Construct_UEnum_MyProject3_EExample_CRC() { return 2117236973U; }
class UScriptStruct* FMyData::StaticStruct()
{
	static class UScriptStruct* Singleton = NULL;
	if (!Singleton)
	{
		extern MYPROJECT3_API uint32 Get_Z_Construct_UScriptStruct_FMyData_CRC();
		Singleton = GetStaticStruct(Z_Construct_UScriptStruct_FMyData, Z_Construct_UPackage__Script_MyProject3(), TEXT("MyData"), sizeof(FMyData), Get_Z_Construct_UScriptStruct_FMyData_CRC());
	}
	return Singleton;
}
static FCompiledInDeferStruct Z_CompiledInDeferStruct_UScriptStruct_FMyData(FMyData::StaticStruct, TEXT("/Script/MyProject3"), TEXT("MyData"), false, nullptr, nullptr);
static struct FScriptStruct_MyProject3_StaticRegisterNativesFMyData
{
	FScriptStruct_MyProject3_StaticRegisterNativesFMyData()
	{
		UScriptStruct::DeferCppStructOps(FName(TEXT("MyData")),new UScriptStruct::TCppStructOps<FMyData>);
	}
} ScriptStruct_MyProject3_StaticRegisterNativesFMyData;
	UScriptStruct* Z_Construct_UScriptStruct_FMyData()
	{
		UPackage* Outer = Z_Construct_UPackage__Script_MyProject3();
		extern uint32 Get_Z_Construct_UScriptStruct_FMyData_CRC();
		static UScriptStruct* ReturnStruct = FindExistingStructIfHotReloadOrDynamic(Outer, TEXT("MyData"), sizeof(FMyData), Get_Z_Construct_UScriptStruct_FMyData_CRC(), false);
		if (!ReturnStruct)
		{
			ReturnStruct = new(EC_InternalUseOnlyConstructor, Outer, TEXT("MyData"), RF_Public|RF_Transient|RF_MarkAsNative) UScriptStruct(FObjectInitializer(), NULL, new UScriptStruct::TCppStructOps<FMyData>, EStructFlags(0x00000001));
			UProperty* NewProp_ActorPtr = new(EC_InternalUseOnlyConstructor, ReturnStruct, TEXT("ActorPtr"), RF_Public|RF_Transient|RF_MarkAsNative) UObjectProperty(CPP_PROPERTY_BASE(ActorPtr, FMyData), 0x0010000000000005, Z_Construct_UClass_AActor_NoRegister());
			UProperty* NewProp_StringValue = new(EC_InternalUseOnlyConstructor, ReturnStruct, TEXT("StringValue"), RF_Public|RF_Transient|RF_MarkAsNative) UStrProperty(CPP_PROPERTY_BASE(StringValue, FMyData), 0x0010000000000005);
			UProperty* NewProp_intValue = new(EC_InternalUseOnlyConstructor, ReturnStruct, TEXT("intValue"), RF_Public|RF_Transient|RF_MarkAsNative) UIntProperty(CPP_PROPERTY_BASE(intValue, FMyData), 0x0010000000000005);
			UProperty* NewProp_EnumValue = new(EC_InternalUseOnlyConstructor, ReturnStruct, TEXT("EnumValue"), RF_Public|RF_Transient|RF_MarkAsNative) UEnumProperty(CPP_PROPERTY_BASE(EnumValue, FMyData), 0x0010000000000005, Z_Construct_UEnum_MyProject3_EExample());
			UProperty* NewProp_EnumValue_Underlying = new(EC_InternalUseOnlyConstructor, NewProp_EnumValue, TEXT("UnderlyingType"), RF_Public|RF_Transient|RF_MarkAsNative) UByteProperty(FObjectInitializer(), EC_CppProperty, 0, 0x0000000000000000);
			ReturnStruct->StaticLink();
#if WITH_METADATA
			UMetaData* MetaData = ReturnStruct->GetOutermost()->GetMetaData();
			MetaData->SetValue(ReturnStruct, TEXT("BlueprintType"), TEXT("true"));
			MetaData->SetValue(ReturnStruct, TEXT("ModuleRelativePath"), TEXT("Cplus_actor.h"));
			MetaData->SetValue(NewProp_ActorPtr, TEXT("Category"), TEXT("MyData"));
			MetaData->SetValue(NewProp_ActorPtr, TEXT("ModuleRelativePath"), TEXT("Cplus_actor.h"));
			MetaData->SetValue(NewProp_StringValue, TEXT("Category"), TEXT("MyData"));
			MetaData->SetValue(NewProp_StringValue, TEXT("ModuleRelativePath"), TEXT("Cplus_actor.h"));
			MetaData->SetValue(NewProp_intValue, TEXT("Category"), TEXT("MyData"));
			MetaData->SetValue(NewProp_intValue, TEXT("ModuleRelativePath"), TEXT("Cplus_actor.h"));
			MetaData->SetValue(NewProp_EnumValue, TEXT("Category"), TEXT("MyData"));
			MetaData->SetValue(NewProp_EnumValue, TEXT("ModuleRelativePath"), TEXT("Cplus_actor.h"));
#endif
		}
		return ReturnStruct;
	}
	uint32 Get_Z_Construct_UScriptStruct_FMyData_CRC() { return 871963214U; }
	void ACplus_actor::StaticRegisterNativesACplus_actor()
	{
	}
	UClass* Z_Construct_UClass_ACplus_actor_NoRegister()
	{
		return ACplus_actor::StaticClass();
	}
	UClass* Z_Construct_UClass_ACplus_actor()
	{
		static UClass* OuterClass = NULL;
		if (!OuterClass)
		{
			Z_Construct_UClass_AActor();
			Z_Construct_UPackage__Script_MyProject3();
			OuterClass = ACplus_actor::StaticClass();
			if (!(OuterClass->ClassFlags & CLASS_Constructed))
			{
				UObjectForceRegistration(OuterClass);
				OuterClass->ClassFlags |= (EClassFlags)0x20900080u;


				UProperty* NewProp_ExampleStructValue = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("ExampleStructValue"), RF_Public|RF_Transient|RF_MarkAsNative) UEnumProperty(CPP_PROPERTY_BASE(ExampleStructValue, ACplus_actor), 0x0010000000000004, Z_Construct_UEnum_MyProject3_EExample());
				UProperty* NewProp_ExampleStructValue_Underlying = new(EC_InternalUseOnlyConstructor, NewProp_ExampleStructValue, TEXT("UnderlyingType"), RF_Public|RF_Transient|RF_MarkAsNative) UByteProperty(FObjectInitializer(), EC_CppProperty, 0, 0x0000000000000000);
				UProperty* NewProp_ExampleEnumValue = new(EC_InternalUseOnlyConstructor, OuterClass, TEXT("ExampleEnumValue"), RF_Public|RF_Transient|RF_MarkAsNative) UEnumProperty(CPP_PROPERTY_BASE(ExampleEnumValue, ACplus_actor), 0x0010000000000014, Z_Construct_UEnum_MyProject3_EExample());
				UProperty* NewProp_ExampleEnumValue_Underlying = new(EC_InternalUseOnlyConstructor, NewProp_ExampleEnumValue, TEXT("UnderlyingType"), RF_Public|RF_Transient|RF_MarkAsNative) UByteProperty(FObjectInitializer(), EC_CppProperty, 0, 0x0000000000000000);
				static TCppClassTypeInfo<TCppClassTypeTraits<ACplus_actor> > StaticCppClassTypeInfo;
				OuterClass->SetCppTypeInfo(&StaticCppClassTypeInfo);
				OuterClass->StaticLink();
#if WITH_METADATA
				UMetaData* MetaData = OuterClass->GetOutermost()->GetMetaData();
				MetaData->SetValue(OuterClass, TEXT("IncludePath"), TEXT("Cplus_actor.h"));
				MetaData->SetValue(OuterClass, TEXT("ModuleRelativePath"), TEXT("Cplus_actor.h"));
				MetaData->SetValue(OuterClass, TEXT("ObjectInitializerConstructorDeclared"), TEXT(""));
				MetaData->SetValue(NewProp_ExampleStructValue, TEXT("Category"), TEXT("Cplus_actor"));
				MetaData->SetValue(NewProp_ExampleStructValue, TEXT("ModuleRelativePath"), TEXT("Cplus_actor.h"));
				MetaData->SetValue(NewProp_ExampleEnumValue, TEXT("Category"), TEXT("Cplus_actor"));
				MetaData->SetValue(NewProp_ExampleEnumValue, TEXT("ModuleRelativePath"), TEXT("Cplus_actor.h"));
#endif
			}
		}
		check(OuterClass->GetClass());
		return OuterClass;
	}
	IMPLEMENT_CLASS(ACplus_actor, 460855416);
	static FCompiledInDefer Z_CompiledInDefer_UClass_ACplus_actor(Z_Construct_UClass_ACplus_actor, &ACplus_actor::StaticClass, TEXT("/Script/MyProject3"), TEXT("ACplus_actor"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ACplus_actor);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
PRAGMA_ENABLE_OPTIMIZATION
